[![Build Status](https://travis-ci.com/kallyas/markdownEditor.svg?branch=master)](https://travis-ci.com/kallyas/markdownEditor)

# MarkdownEditor
 Lightweight realtime markdown viewer and editor - Simple, clean and beautiful

To do

- [X] Create server
- [X] Create Database
- [ ] Code of Conduct
- [X] Icons
- [X] Orientation

[![Run on Repl.it](https://repl.it/badge/github/kallyas/markdownEditor)](https://repl.it/github/kallyas/markdownEditor)